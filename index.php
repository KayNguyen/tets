<?php

namespace App\Http\Models;

use App\Elibs\Debug;
use App\Elibs\eCache;
use App\Elibs\Helper;
use Illuminate\Support\Facades\DB;
use \PDO;
use PDOException;


/**
 * Class Comments
 * @package App\Http\Models
 *
 * id: int, autoincrement
 * title: tiêu đề nếu có (tối đa 127 ký tự)
 * content: Nội dung bình luận (tối đa 512 ký thôi) => không hỗ trợ nội dung là html (text base) plaintext
 * user_id: nếu có
 * user_name: Tên hiển thị
 * post_id: id của bài viết
 * chapter_id: id của chương
 * created_at: thời gian bình luận
 *
 */
class Comments
{
    public $timestamps = false;
    const table_name = 'io_comments';
    protected $table = self::table_name;
    static $unguarded = true;
    function __construct() {
        $post_id = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : false;
        if ($post_id) {
            $post_id = explode('-', explode('.html', $post_id)[0]);
            $post_id = substr($post_id[sizeof($post_id) -1], 2);
        }
        $chapter_id = 1000;
        $content = isset($_POST['comment_content']) ? $_POST['comment_content'] : false;
        $user_name = isset($_POST['comment_user']) ? $_POST['comment_user'] : false;
        $ip = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : false;     /*getIP*/
        $client_info = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : false;
        try{

            $conn = new PDO('mysql:host=103.81.85.103;dbname=doctruyen_tv;charset=utf8', 'doctruyen_tv', '');

        }catch (PDOException $e) {

            throw new Exception('No connect to database!');

        }

        $sth = $conn->prepare('SELECT * FROM doctruyen_tv.io_comments');

        $sth->execute();

        $result = $sth->fetchAll(PDO::FETCH_OBJ);

        $sth = $conn->prepare('INSERT INTO `io_comments`(`post_id`, `chapter_id`, `content`, `user_name`, `user_id`, `created_at`, `ip`, `client_info`) VALUES (:post_id, :chapter_id, :content, :user_name, :user_id, :created_at, :ip, :client_info)');

        $created_at = date('Y-m-d H:i:s', time());

        $sth->execute(array(

            ':post_id' => $post_id,

            ':chapter_id' => $chapter_id,

            ':content' => $content,

            ':user_name' => $user_name,

            ':user_id' => '',

            ':created_at' => $created_at,

            ':ip' => $ip,

            ':client_info' => $client_info

        ));

        $stmt = $conn->prepare('SELECT * FROM doctruyen_tv.io_comments');

        //Thiết lập kiểu dữ liệu trả về
        $stmt->setFetchMode(PDO::FETCH_ASSOC);

        //Gán giá trị và thực thi
        $stmt->execute();

        //Hiển thị kết quả, vòng lặp sau đây sẽ dừng lại khi đã duyệt qua toàn bộ kết quả
//        while($row = $stmt->fetch()) {
//            echo $row['user'] , "<br>";
//            echo $row['content'] , "<br>";
//            echo $row['email'] , "<br>";
//            $timestamp = strtotime($row['create_time']);        /*strtotime() convert datetiem to timestamp*/
//            $tim = time() - $timestamp;
//            echo $tim , "<br>";
//        }

    }
}
